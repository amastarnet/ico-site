webpackJsonp([0],{

/***/ 214:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(276)

var Component = __webpack_require__(147)(
  /* script */
  __webpack_require__(260),
  /* template */
  __webpack_require__(300),
  /* scopeId */
  "data-v-e0db1226",
  /* cssModules */
  null
)
Component.options.__file = "/var/www/devico.amastar.net/src/views/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e0db1226", Component.options)
  } else {
    hotAPI.reload("data-v-e0db1226", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 219:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__(225);
var isBuffer = __webpack_require__(244);

/*global toString:true*/

// utils is a library of generic helper functions non-specific to axios

var toString = Object.prototype.toString;

/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
function isArray(val) {
  return toString.call(val) === '[object Array]';
}

/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}

/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
function isFormData(val) {
  return (typeof FormData !== 'undefined') && (val instanceof FormData);
}

/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
function isArrayBufferView(val) {
  var result;
  if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
    result = ArrayBuffer.isView(val);
  } else {
    result = (val) && (val.buffer) && (val.buffer instanceof ArrayBuffer);
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
function isString(val) {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
function isNumber(val) {
  return typeof val === 'number';
}

/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
function isUndefined(val) {
  return typeof val === 'undefined';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
function isObject(val) {
  return val !== null && typeof val === 'object';
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
function isDate(val) {
  return toString.call(val) === '[object Date]';
}

/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
function isFile(val) {
  return toString.call(val) === '[object File]';
}

/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
function isFunction(val) {
  return toString.call(val) === '[object Function]';
}

/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}

/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}

/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
function trim(str) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}

/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  navigator.product -> 'ReactNative'
 */
function isStandardBrowserEnv() {
  if (typeof navigator !== 'undefined' && navigator.product === 'ReactNative') {
    return false;
  }
  return (
    typeof window !== 'undefined' &&
    typeof document !== 'undefined'
  );
}

/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  }

  // Force an array if not already something iterable
  if (typeof obj !== 'object') {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}

/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function merge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = merge(result[key], val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */
function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isBuffer: isBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  extend: extend,
  trim: trim
};


/***/ }),

/***/ 220:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

var utils = __webpack_require__(219);
var normalizeHeaderName = __webpack_require__(241);

var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter;
  if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = __webpack_require__(221);
  } else if (typeof process !== 'undefined') {
    // For node use HTTP adapter
    adapter = __webpack_require__(221);
  }
  return adapter;
}

var defaults = {
  adapter: getDefaultAdapter(),

  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Content-Type');
    if (utils.isFormData(data) ||
      utils.isArrayBuffer(data) ||
      utils.isBuffer(data) ||
      utils.isStream(data) ||
      utils.isFile(data) ||
      utils.isBlob(data)
    ) {
      return data;
    }
    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }
    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }
    if (utils.isObject(data)) {
      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
      return JSON.stringify(data);
    }
    return data;
  }],

  transformResponse: [function transformResponse(data) {
    /*eslint no-param-reassign:0*/
    if (typeof data === 'string') {
      try {
        data = JSON.parse(data);
      } catch (e) { /* Ignore */ }
    }
    return data;
  }],

  /**
   * A timeout in milliseconds to abort a request. If set to 0 (default) a
   * timeout is not created.
   */
  timeout: 0,

  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',

  maxContentLength: -1,

  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};

defaults.headers = {
  common: {
    'Accept': 'application/json, text/plain, */*'
  }
};

utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  defaults.headers[method] = {};
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});

module.exports = defaults;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(148)))

/***/ }),

/***/ 221:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(219);
var settle = __webpack_require__(233);
var buildURL = __webpack_require__(236);
var parseHeaders = __webpack_require__(242);
var isURLSameOrigin = __webpack_require__(240);
var createError = __webpack_require__(224);
var btoa = (typeof window !== 'undefined' && window.btoa && window.btoa.bind(window)) || __webpack_require__(235);

module.exports = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest();
    var loadEvent = 'onreadystatechange';
    var xDomain = false;

    // For IE 8/9 CORS support
    // Only supports POST and GET calls and doesn't returns the response headers.
    // DON'T do this for testing b/c XMLHttpRequest is mocked, not XDomainRequest.
    if ("production" !== 'test' &&
        typeof window !== 'undefined' &&
        window.XDomainRequest && !('withCredentials' in request) &&
        !isURLSameOrigin(config.url)) {
      request = new window.XDomainRequest();
      loadEvent = 'onload';
      xDomain = true;
      request.onprogress = function handleProgress() {};
      request.ontimeout = function handleTimeout() {};
    }

    // HTTP basic authentication
    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password || '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true);

    // Set the request timeout in MS
    request.timeout = config.timeout;

    // Listen for ready state
    request[loadEvent] = function handleLoad() {
      if (!request || (request.readyState !== 4 && !xDomain)) {
        return;
      }

      // The request errored out and we didn't get a response, this will be
      // handled by onerror instead
      // With one exception: request that using file: protocol, most browsers
      // will return status as 0 even though it's a successful request
      if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
        return;
      }

      // Prepare the response
      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
      var response = {
        data: responseData,
        // IE sends 1223 instead of 204 (https://github.com/axios/axios/issues/201)
        status: request.status === 1223 ? 204 : request.status,
        statusText: request.status === 1223 ? 'No Content' : request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };

      settle(resolve, reject, response);

      // Clean up request
      request = null;
    };

    // Handle low level network errors
    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config, null, request));

      // Clean up request
      request = null;
    };

    // Handle timeout
    request.ontimeout = function handleTimeout() {
      reject(createError('timeout of ' + config.timeout + 'ms exceeded', config, 'ECONNABORTED',
        request));

      // Clean up request
      request = null;
    };

    // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.
    if (utils.isStandardBrowserEnv()) {
      var cookies = __webpack_require__(238);

      // Add xsrf header
      var xsrfValue = (config.withCredentials || isURLSameOrigin(config.url)) && config.xsrfCookieName ?
          cookies.read(config.xsrfCookieName) :
          undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    }

    // Add headers to the request
    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    }

    // Add withCredentials to request if needed
    if (config.withCredentials) {
      request.withCredentials = true;
    }

    // Add responseType to request if needed
    if (config.responseType) {
      try {
        request.responseType = config.responseType;
      } catch (e) {
        // Expected DOMException thrown by browsers not compatible XMLHttpRequest Level 2.
        // But, this can be suppressed for 'json' type as it can be parsed by default 'transformResponse' function.
        if (config.responseType !== 'json') {
          throw e;
        }
      }
    }

    // Handle progress if needed
    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    }

    // Not all browsers support upload events
    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken) {
      // Handle cancellation
      config.cancelToken.promise.then(function onCanceled(cancel) {
        if (!request) {
          return;
        }

        request.abort();
        reject(cancel);
        // Clean up request
        request = null;
      });
    }

    if (requestData === undefined) {
      requestData = null;
    }

    // Send the request
    request.send(requestData);
  });
};


/***/ }),

/***/ 222:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */
function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;

module.exports = Cancel;


/***/ }),

/***/ 223:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};


/***/ }),

/***/ 224:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var enhanceError = __webpack_require__(232);

/**
 * Create an Error with the specified message, config, error code, request and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The created error.
 */
module.exports = function createError(message, config, code, request, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, request, response);
};


/***/ }),

/***/ 225:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }
    return fn.apply(thisArg, args);
  };
};


/***/ }),

/***/ 226:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(227);

/***/ }),

/***/ 227:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(219);
var bind = __webpack_require__(225);
var Axios = __webpack_require__(229);
var defaults = __webpack_require__(220);

/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */
function createInstance(defaultConfig) {
  var context = new Axios(defaultConfig);
  var instance = bind(Axios.prototype.request, context);

  // Copy axios.prototype to instance
  utils.extend(instance, Axios.prototype, context);

  // Copy context to instance
  utils.extend(instance, context);

  return instance;
}

// Create the default instance to be exported
var axios = createInstance(defaults);

// Expose Axios class to allow class inheritance
axios.Axios = Axios;

// Factory for creating new instances
axios.create = function create(instanceConfig) {
  return createInstance(utils.merge(defaults, instanceConfig));
};

// Expose Cancel & CancelToken
axios.Cancel = __webpack_require__(222);
axios.CancelToken = __webpack_require__(228);
axios.isCancel = __webpack_require__(223);

// Expose all/spread
axios.all = function all(promises) {
  return Promise.all(promises);
};
axios.spread = __webpack_require__(243);

module.exports = axios;

// Allow use of default import syntax in TypeScript
module.exports.default = axios;


/***/ }),

/***/ 228:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Cancel = __webpack_require__(222);

/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */
function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;
  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });

  var token = this;
  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel(message);
    resolvePromise(token.reason);
  });
}

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};

/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */
CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

module.exports = CancelToken;


/***/ }),

/***/ 229:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var defaults = __webpack_require__(220);
var utils = __webpack_require__(219);
var InterceptorManager = __webpack_require__(230);
var dispatchRequest = __webpack_require__(231);

/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */
function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}

/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */
Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = utils.merge({
      url: arguments[0]
    }, arguments[1]);
  }

  config = utils.merge(defaults, {method: 'get'}, this.defaults, config);
  config.method = config.method.toLowerCase();

  // Hook up interceptors middleware
  var chain = [dispatchRequest, undefined];
  var promise = Promise.resolve(config);

  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    chain.unshift(interceptor.fulfilled, interceptor.rejected);
  });

  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    chain.push(interceptor.fulfilled, interceptor.rejected);
  });

  while (chain.length) {
    promise = promise.then(chain.shift(), chain.shift());
  }

  return promise;
};

// Provide aliases for supported request methods
utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url
    }));
  };
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, data, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});

module.exports = Axios;


/***/ }),

/***/ 230:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(219);

function InterceptorManager() {
  this.handlers = [];
}

/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */
InterceptorManager.prototype.use = function use(fulfilled, rejected) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected
  });
  return this.handlers.length - 1;
};

/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */
InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};

/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */
InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;


/***/ }),

/***/ 231:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(219);
var transformData = __webpack_require__(234);
var isCancel = __webpack_require__(223);
var defaults = __webpack_require__(220);
var isAbsoluteURL = __webpack_require__(239);
var combineURLs = __webpack_require__(237);

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }
}

/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */
module.exports = function dispatchRequest(config) {
  throwIfCancellationRequested(config);

  // Support baseURL config
  if (config.baseURL && !isAbsoluteURL(config.url)) {
    config.url = combineURLs(config.baseURL, config.url);
  }

  // Ensure headers exist
  config.headers = config.headers || {};

  // Transform request data
  config.data = transformData(
    config.data,
    config.headers,
    config.transformRequest
  );

  // Flatten headers
  config.headers = utils.merge(
    config.headers.common || {},
    config.headers[config.method] || {},
    config.headers || {}
  );

  utils.forEach(
    ['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
    function cleanHeaderConfig(method) {
      delete config.headers[method];
    }
  );

  var adapter = config.adapter || defaults.adapter;

  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config);

    // Transform response data
    response.data = transformData(
      response.data,
      response.headers,
      config.transformResponse
    );

    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config);

      // Transform response data
      if (reason && reason.response) {
        reason.response.data = transformData(
          reason.response.data,
          reason.response.headers,
          config.transformResponse
        );
      }
    }

    return Promise.reject(reason);
  });
};


/***/ }),

/***/ 232:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The error.
 */
module.exports = function enhanceError(error, config, code, request, response) {
  error.config = config;
  if (code) {
    error.code = code;
  }
  error.request = request;
  error.response = response;
  return error;
};


/***/ }),

/***/ 233:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var createError = __webpack_require__(224);

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;
  // Note: status is not exposed by XDomainRequest
  if (!response.status || !validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError(
      'Request failed with status code ' + response.status,
      response.config,
      null,
      response.request,
      response
    ));
  }
};


/***/ }),

/***/ 234:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(219);

/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */
module.exports = function transformData(data, headers, fns) {
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn(data, headers);
  });

  return data;
};


/***/ }),

/***/ 235:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// btoa polyfill for IE<10 courtesy https://github.com/davidchambers/Base64.js

var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

function E() {
  this.message = 'String contains an invalid character';
}
E.prototype = new Error;
E.prototype.code = 5;
E.prototype.name = 'InvalidCharacterError';

function btoa(input) {
  var str = String(input);
  var output = '';
  for (
    // initialize result and counter
    var block, charCode, idx = 0, map = chars;
    // if the next str index does not exist:
    //   change the mapping table to "="
    //   check if d has no fractional digits
    str.charAt(idx | 0) || (map = '=', idx % 1);
    // "8 - idx % 1 * 8" generates the sequence 2, 4, 6, 8
    output += map.charAt(63 & block >> 8 - idx % 1 * 8)
  ) {
    charCode = str.charCodeAt(idx += 3 / 4);
    if (charCode > 0xFF) {
      throw new E();
    }
    block = block << 8 | charCode;
  }
  return output;
}

module.exports = btoa;


/***/ }),

/***/ 236:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(219);

function encode(val) {
  return encodeURIComponent(val).
    replace(/%40/gi, '@').
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%20/g, '+').
    replace(/%5B/gi, '[').
    replace(/%5D/gi, ']');
}

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];

    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      } else {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};


/***/ }),

/***/ 237:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */
module.exports = function combineURLs(baseURL, relativeURL) {
  return relativeURL
    ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
    : baseURL;
};


/***/ }),

/***/ 238:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(219);

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs support document.cookie
  (function standardBrowserEnv() {
    return {
      write: function write(name, value, expires, path, domain, secure) {
        var cookie = [];
        cookie.push(name + '=' + encodeURIComponent(value));

        if (utils.isNumber(expires)) {
          cookie.push('expires=' + new Date(expires).toGMTString());
        }

        if (utils.isString(path)) {
          cookie.push('path=' + path);
        }

        if (utils.isString(domain)) {
          cookie.push('domain=' + domain);
        }

        if (secure === true) {
          cookie.push('secure');
        }

        document.cookie = cookie.join('; ');
      },

      read: function read(name) {
        var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
        return (match ? decodeURIComponent(match[3]) : null);
      },

      remove: function remove(name) {
        this.write(name, '', Date.now() - 86400000);
      }
    };
  })() :

  // Non standard browser env (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return {
      write: function write() {},
      read: function read() { return null; },
      remove: function remove() {}
    };
  })()
);


/***/ }),

/***/ 239:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */
module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
};


/***/ }),

/***/ 240:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(219);

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
  (function standardBrowserEnv() {
    var msie = /(msie|trident)/i.test(navigator.userAgent);
    var urlParsingNode = document.createElement('a');
    var originURL;

    /**
    * Parse a URL to discover it's components
    *
    * @param {String} url The URL to be parsed
    * @returns {Object}
    */
    function resolveURL(url) {
      var href = url;

      if (msie) {
        // IE needs attribute set twice to normalize properties
        urlParsingNode.setAttribute('href', href);
        href = urlParsingNode.href;
      }

      urlParsingNode.setAttribute('href', href);

      // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
      return {
        href: urlParsingNode.href,
        protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
        host: urlParsingNode.host,
        search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
        hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
        hostname: urlParsingNode.hostname,
        port: urlParsingNode.port,
        pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
                  urlParsingNode.pathname :
                  '/' + urlParsingNode.pathname
      };
    }

    originURL = resolveURL(window.location.href);

    /**
    * Determine if a URL shares the same origin as the current location
    *
    * @param {String} requestURL The URL to test
    * @returns {boolean} True if URL shares the same origin, otherwise false
    */
    return function isURLSameOrigin(requestURL) {
      var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
      return (parsed.protocol === originURL.protocol &&
            parsed.host === originURL.host);
    };
  })() :

  // Non standard browser envs (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return function isURLSameOrigin() {
      return true;
    };
  })()
);


/***/ }),

/***/ 241:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(219);

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};


/***/ }),

/***/ 242:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(219);

// Headers whose duplicates are ignored by node
// c.f. https://nodejs.org/api/http.html#http_message_headers
var ignoreDuplicateOf = [
  'age', 'authorization', 'content-length', 'content-type', 'etag',
  'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since',
  'last-modified', 'location', 'max-forwards', 'proxy-authorization',
  'referer', 'retry-after', 'user-agent'
];

/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */
module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) { return parsed; }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
        return;
      }
      if (key === 'set-cookie') {
        parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
      } else {
        parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
      }
    }
  });

  return parsed;
};


/***/ }),

/***/ 243:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */
module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};


/***/ }),

/***/ 244:
/***/ (function(module, exports) {

/*!
 * Determine if an object is a Buffer
 *
 * @author   Feross Aboukhadijeh <https://feross.org>
 * @license  MIT
 */

// The _isBuffer check is for Safari 5-7 support, because it's missing
// Object.prototype.constructor. Remove this eventually
module.exports = function (obj) {
  return obj != null && (isBuffer(obj) || isSlowBuffer(obj) || !!obj._isBuffer)
}

function isBuffer (obj) {
  return !!obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
}

// For Node v0.10 support. Remove this eventually.
function isSlowBuffer (obj) {
  return typeof obj.readFloatLE === 'function' && typeof obj.slice === 'function' && isBuffer(obj.slice(0, 0))
}


/***/ }),

/***/ 245:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "c72efa874a1444c7f0932f1fd9cc0a2e.png";

/***/ }),

/***/ 246:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "9a4f313d2f892653a9372bd279f8dc9e.png";

/***/ }),

/***/ 247:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAdCAMAAABymtfJAAAAAXNSR0IB2cksfwAAAORQTFRFAAAA/8xN99dY9dhY9tFS+dZT+dVU+dVT+dVU+tVV+dVU+dVU+tVV+dVU99ZS+dVU+NZV+NVV+NVU+NVV+dVU+dVU+dVU+dVU99RS/89Q+dVT+NRU/9dR+dVU+NVU+dVT+dRU+tZS+dVU+tVV+dNR+NNU+tRT+dVU+dVU+tZV/6pV+dVT+NZU+NRV+dZU+dVV+dVU+dVU/9VV+dVU+ddV+dVU+dRV+NVU+dZV/9Ja+9dV+dVU+dNT9tlV+dVU/9VV+tVT+dRV+dZU//+A+tVU+NVU+tRT+NVU+dZU+dRU+tVT/9hORRwjyQAAAEx0Uk5TAAogGhxQ/9DgYP7jvn8f+XVIwMT17/BVQRDWvxP76eLeOJ5mKUyQ9udjA59wcoiv7skGgC3UVMJRETn8LhtbEjFapgJh61/o0adiDbvV0AkAAADGSURBVHictdLVFoJAFIXh4wiDjYEtdnd3d7z/+8ghBEcvdV/9fIsFLAAAZyPEBnbCwdt4SnkQqOOH6qTmXG6P1/ehONGPGgiGJDySwhHdo9pFYtg8QJxPYCU1TekKwMlKpTWNvxQySmVZzWHlLVogxVK5okSVWNSYXKt/UUoFztRGs9XudFXuMXfrD1QeMk8GI+wxqxPsKdbM1PkCewmwIjWs9YZsd3sRM30AMF6VuaNyKmunM75f67e4XG/3xz+/vDr2P3sCLJkTJIvWyuMAAAAASUVORK5CYII="

/***/ }),

/***/ 248:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAYCAMAAAD57OxYAAAAAXNSR0IB2cksfwAAAIpQTFRFAAAA99hV+dVV+NVU+dVU+dVU/79A+NVU+dVU+dVU+dRT+NVU+dRT+dVU+NZU+dVU+NZV99VV+ddV+tVU+tRV+dRU+tVT99VV+dNT+dZR+dVU+NRT+NVV+dVT+NVU+NVV+dVU+dVU+9dV+dNU+dVV+dVU+9NU+dVU+tVU+NZV+dVU+dZV+dZU+NNU5P1rcgAAAC50Uk5TACG46/KxBOj/zlPDis+UsEVCLZI2gzEeLiyka8THwsHU1TlStYU6f5FLnVGIRqLS9NgAAAB2SURBVHictdBZEoIwFETRJwoEQQSZwuRAQBn3vz3SkiIroL/u+XmpCpHcyThf5Ey0ZbP/HOC6NXMBT+EG+Kh7ED6ACIhpWwKkqIznBVDmPCOX7auo1qjpqfGit8aHGiFaVCfEFwd/AFfv9MBwPEZgUpjxT4uMFVX1Cwjqi63pAAAAAElFTkSuQmCC"

/***/ }),

/***/ 249:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAUCAMAAAC+oj0CAAAAAXNSR0IB2cksfwAAAN5QTFRFAAAA+NRV+dVU+NVU+dRV+tVU+dVU+9VU+dVT+tVT+NZV+dVU+dVT+dVU+dVU+NZV/9VV+dRT+dZU+tVU9dZS+9VV/9tb+dVU+NRT+dVU+tRT+dZT9dhY+tRV+NVU+dVU+tVU+NVV/6pV89FR+dVU+dNT+dZV+dVU+tVU+dRV+dVU+9dV+dVU+NNU+NJT+dVU+tZU+NRV+dVV+NVV/9VV+dVU+dNU+dZV/9Ja+dVU+dVU+dRU885V+dVU+NZV+tVT+tNT+dZU+tVT9NVV+dVU+dVT+NRV+NZT+dZU+dVU7TE+qAAAAEp0Uk5TAGzJw1ph/z3NjXVPhNrSbwyKgowZPA5bnKqQUBo2c4C8wQMW/S5R/L1U5Dn2TCLXjnKsSBK0UlcR+bFYFfppaDR2YhjIxyRExYsI1KpPAAAAsklEQVR4nJXOxxKCQAwG4LgqimJdsCv23gti7+39X8hkEEfc8WAOyb/fIVkAF3N7QCivhOUT2E8sCxwgDopblFA4IuqPisbiCldB45zLCUim0pkscY5256FAQy9Sl0plgEoVQw3q2BvSq5pvblnQtkbHwd0e9AcUhg4e4S2NwtjBE+QpXZjZbBDPkc0FhqXNK5vXm0/e/sW7L1bpvYcDjSPyicIZzAtj7AoGdnajn9wxPJ7yNxcMY//uRwAAAABJRU5ErkJggg=="

/***/ }),

/***/ 250:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAUCAMAAACknt2MAAAAAXNSR0IB2cksfwAAAPxQTFRFAAAA+tVU+dVU+dVU+tRU+dVU+dZU+dVU+dVU+dVU+NVV+dVT+dVT+dVU/89Q/9hO+dVU+tVU+NRT+NRT99VV+dVU+NNU+dVT+dVU+NVT+dRV+dVU+dVU+tVU+dVV+dVU+NVU+dZU+dRU+tVU+dRU+NVV+dZU/9tb/99g+NRV+NVU+tRS+tRR+dVV+dVU+dRU+tVT+dVT+dVT+NVU+tVU+dVU+NVU+NRU+dNU+dVU+dRT/9VV+NRU+dVU//8A+dVT+tVV/8xN+NRU+NVU+dRU99dY+dVU+dVT+dZT9tNY+dVT+dVU+dRT+dRT+dVU+NVT+NVU+dVU+dRU+9ZSOjjqCgAAAFR0Uk5TAJHv7o+Lxf/g206B2f4QDfWMTUdCpEx+eplU5/G6tezroK69rcR8DghswjUvKtenaNDWw5LywJVS81MSv+MB02AKm0mzIPDfex2E+YqoT5Pof1g+iHRePgAAAQhJREFUeJxlkQlTwjAQhV8BWQ0EvM+golaKeEu8EU+8xeP//xd3k7Zj65tMJrvf7r5kgqBQ9CqUwBpJwwBlSjQqaCwNy1CVKmkRUQ2o6zgYn1BcOEmk3ZoCpqVe0IyMwKxHmuaAeX/UtODQIhcuGc41gGWilVUpa6ZobZ2RAYpEG2EWlTZlUiviY3sri4KO+He22WhnN4v2IHdp7xMdIOfVwCGj8IioC5tFxziRBOsUZ3l07l5HdJHvukTNP7WHq38IfYeaiP4g5VHXmV3jxqFbh+4Y3SM2ewAGSZeq8CR6fEJdUA/PL6+CBm8KYfxzEaq8v+Mj+coQQ2tY9rOFL2vsN36sSxg7/AVr3iAErkOUHQAAAABJRU5ErkJggg=="

/***/ }),

/***/ 251:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ea7912a1e145d8e18d30b8f43b0cd0cb.png";

/***/ }),

/***/ 252:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAWCAMAAADto6y6AAAAAXNSR0IB2cksfwAAAXFQTFRFAAAA+dNR+dRT+dVU+dVU+dVU+NNU+dRU+dVU/9dR+NVU+dVU+NVU+dZT/9VV+dVU+tNT+NVT+NRU+NRT+dVU+dRT+NVV+NVU+tZS885V89FR+dVU9tNY+dVU+NVU+dVT+NZV/9Ja+NdX+tRU+NZT+dVT+9ZS+dZU+NZT+dVT+dVU+NZV+NRT/8xN+dVU+dVU+NhV+tRV+dVU+dVU+dVU99VU+dRU+dVU+dVU+tZV/9tJ+tVT+dZU+NVU+dVU+tRR+dRV+dVT/89Q+dVU+NJT+dVV+dVU99hV99NU+dVV+dVT+dVU/9VV+dVT+dVU+tVU+dVU+dVT/9Fd+NVV9NNZ//8A+dVU+dVU/9hO+dVU+dRU+dVU+tVT+9VV+dVV/91V+dVU+dVT+dNU+NZV+tVV+9dV+dZU+NRT+dVU+9NU+dVU+tVU+dVU99VV9dZS+NZV+dVU+NVU+tVV+dVU+tZU+dVU+tRS+NRU+tVU+dRT+dVUkTO3JgAAAHt0Uk5TACmK/P/kTLPgE3Pd6XsMozSZv03yWcGXMhUW5R1/69xvESZkRKU+pkp+hnVxCs/2JzbUW6tDiffOYweN0eqAL1TiELcir+YhQKxc9BLZeWG0zQtIFwHhqg3xWPg3PLUP/tBSab4/iJzYOvu92h4ZS8noYP1epDWVkVOebM16CgAAATRJREFUeJxtkQdTAjEQhRfuCdJEFAtWFERBsGM5xa6ooKigYu+99/Lr3dx5nIiZSfbb9zaZZENUOAxGCUWFsskMoBh/VIvVBtgdJc58o9TFxWXlTO7fRkUly1XVRB6qQa2m1tU3AI3eJqJmm4/8aFHlQCsXm9sEBhEiakeYMeLrADqNXULu7kEvhz5EKdI/AAwOyUKWh4GAADssNILY6Jh64PgEJAUnESMCpgyKPD0DzMYVnMM80YIbSLiiSasELHrUrWEs8ZpMLfOVEjxXVn/u7kRahUxKEk9e0x61joyGG2zEtSSLzVw7toBtmfw7SrKLvZyxf4BDbgSORHKMoN7BE4RO+TicMZ/DpBteXADpyytcZ+kGt7pxx9X33OQHPD495/3Sy+ubEt+54oP+G59fDhG+AQrZJCHxPbpKAAAAAElFTkSuQmCC"

/***/ }),

/***/ 253:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAXCAMAAADA1rRbAAAAAXNSR0IB2cksfwAAARdQTFRFAAAA+9dV+dVU+dRU+dVU+dVT+NRU+dZR+dJT99RS8tlZ+dVT+dVU+dVU+tRU+9ZS+NRT+9VU+NVU+dRU+tZV+dVU+NRT+dRU+dVU/6pV/8xN+dVU+dVU+dZU+dVU+dRU99NU+dVU+dVU+tVT+dRT+tRR+dRU+dVU9NNZ/8ZV+dVU+dVU+tVV+NZT+NRT+dVT+9NU+dVU+dZU+dVT+NZU+dNU+tZS+dVU+dVU+dRU+tZU+dVU+dVU+NNX+dVU/9VV+dVU/79A+dVU+dVU+dVU+dRU+dVU9tlV+dVU+tdV+dVU+tVU+tVT+NZV+dZU+dVT+dNT99ZS+NVV+dVT+dZU99VV+tVU+dRU+ddV99VU+dVU+NZU+9RSgnd0TAAAAF10Uk5TADmk3u3ZmywoQRR+ev9kPpw9SaFd/k13qwMK+7CCtH1AW/03Uy+JzBcJ+bdmJWtcOt25ympSMoX4WI7P9yPzBrYEzvz2rX8b8jPJkY1LxaIuH0jidh6Ssy1D4Zo7rVW4owAAAQtJREFUeJxt0tdCwjAUBuDDFCs/IlAHKG5BBDcFBEFRFBUH4tb3fw6blKRJy7lq853RJiHyRiAYCkeiU/ZTbJrImNEwDicSs0nMEaWQVjADmCxYwrz9vgAsLgnM5hxkvLySJ1q1syJrY12HUHMD2CTa4mPS21x3JJooFO2F3RL4mL1yhWhfKnDA8w95N9bg6PjELcUpw6pVq8vPVBqjwfQMzZbpD8Bieq5WKNru8LnByXrh/ONld0I1kBE7VO5e+fS6M8ZKvFfyld6I0lt/Z/Tl1tMdPA6klHOz7jUFHvQbURzkFOx57svjk4LPctl4MYavgxHcU8ebW/T+UYcWn19a0+/kT1/E6PdPpX9sNS8gB0QzlAAAAABJRU5ErkJggg=="

/***/ }),

/***/ 260:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    data() {
        return {
            showCTO: false,
            showCEO: false,
            email: '',
            sendemail: '',
            name: '',
            message: '',
            charOptions: ''
        };
    },
    methods: {
        async submit() {
            const email = this.email;
            if (this.email === '') {
                this.$Modal.error({
                    tittle: "ERROR",
                    content: '<b2>Email was not be empty</b2>',
                    okText: 'OK'
                });
            } else {
                try {
                    const res = await __WEBPACK_IMPORTED_MODULE_0_axios___default.a.get('https://' + window.location.hostname + ':3000/api/findsub', {
                        params: {
                            email: email
                        }
                    });
                    console.log(res);
                    if (res.data.data.length > 0) {
                        this.$Modal.error({
                            tittle: "ERROR",
                            content: '<b2>Email was subscribed</b2>',
                            okText: 'OK'
                        });
                    } else {
                        try {
                            const add = await __WEBPACK_IMPORTED_MODULE_0_axios___default.a.post('https://' + window.location.hostname + ':3000/api/sub', {
                                email: email
                            });
                            this.$Modal.success({
                                content: '<b2>YOU was subscribed</b2>',
                                okText: 'OK'
                            });
                        } catch (error) {
                            console.log(error);
                        }
                    }
                } catch (error) {
                    console.log(error);
                }
            }
            this.email = "";
        },
        register() {
            this.$router.push({ path: 'register' });
        },
        showWhitepaper() {
            {
                location.href = "./dist/Whitepaper.pdf";
            }
        },
        showTerms() {
            {
                location.href = "./dist/TermsofSale.pdf";
            }
        },
        submitmail() {
            if (this.sendemail === '') {
                this.$Modal.error({
                    tittle: "ERROR",
                    content: '<b2>Email was not be empty</b2>',
                    okText: 'OK'
                });
            } else if (this.name === '') {
                this.$Modal.error({
                    tittle: "ERROR",
                    content: '<b2>Name was not be empty</b2>',
                    okText: 'OK'
                });
            } else if (this.message == '') {
                this.$Modal.error({
                    tittle: "ERROR",
                    content: '<b2>Message was not be empty</b2>',
                    okText: 'OK'
                });
            } else {

                this.$Modal.info({
                    content: '<b2>Thanks for Email</b2>',
                    okText: 'OK'
                });
            }
        }
    },
    computed: {
        user: function () {
            return localStorage.getItem('email') !== '';
        }
    }
});

/***/ }),

/***/ 276:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 277:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "2eaad6d29ab67c851b9f0637438c9fff.png";

/***/ }),

/***/ 278:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "90e1f7eeca0120e1fb4f434188b8eff4.png";

/***/ }),

/***/ 279:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "f9ec5cf8e46cbd2895779c1c3bcbf735.png";

/***/ }),

/***/ 280:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "51b59d58973a0e28e257239647e66b4e.png";

/***/ }),

/***/ 281:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ac4034845108275d88eb6a66426bf9c7.png";

/***/ }),

/***/ 282:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "89ad65eeaba7021e57974aafcfb2245f.png";

/***/ }),

/***/ 283:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "c81cdb027d9169da1794d12985f7ae69.png";

/***/ }),

/***/ 284:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "3736faaf13c4c1747b2fca8f1cab35d2.png";

/***/ }),

/***/ 285:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "f5e74b6a167b2e60d884bdbc4de6fda0.png";

/***/ }),

/***/ 286:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "e458c3ee06a53bd5105248c56247b793.png";

/***/ }),

/***/ 287:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "1483a8090da4a85018fd07f85b4d80ae.png";

/***/ }),

/***/ 288:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "b9917f2f919e7da47b7ff1550cf4fa7c.png";

/***/ }),

/***/ 289:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "491e4fc2cf52cb27f73b80d83291074e.png";

/***/ }),

/***/ 300:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrapper index"
  }, [_c('header', [_c('div', {
    staticClass: "container flex al_center"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "block flex"
  }, [_c('ul', {
    staticClass: "menu flex al_center"
  }, [_vm._m(1), _vm._v(" "), _c('li', [_c('a', {
    directives: [{
      name: "smooth-scroll",
      rawName: "v-smooth-scroll"
    }],
    attrs: {
      "href": "#about"
    }
  }, [_vm._v("About")])]), _vm._v(" "), _c('li', [_c('a', {
    directives: [{
      name: "smooth-scroll",
      rawName: "v-smooth-scroll"
    }],
    attrs: {
      "href": "#team"
    }
  }, [_vm._v("Team")])]), _vm._v(" "), _c('li', [_c('a', {
    directives: [{
      name: "smooth-scroll",
      rawName: "v-smooth-scroll"
    }],
    attrs: {
      "href": "#roadmap"
    }
  }, [_vm._v("Roadmap")])]), _vm._v(" "), _c('li', [_c('a', {
    directives: [{
      name: "smooth-scroll",
      rawName: "v-smooth-scroll"
    }],
    attrs: {
      "href": "#contact"
    }
  }, [_vm._v("Contact")])])]), _vm._v(" "), _c('ul', {
    staticClass: "login flex"
  }, [(_vm.user) ? _c('li', [_c('a', {
    attrs: {
      "href": "details"
    }
  }, [_vm._v("Cabinet")])]) : _vm._e(), _vm._v(" "), (!_vm.user) ? _c('li', [_c('a', {
    attrs: {
      "href": "login"
    }
  }, [_vm._v("Login")])]) : _vm._e()])])])]), _vm._v(" "), _vm._m(2), _vm._v(" "), _c('section', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "div_1"
  }, [_c('div', {
    staticClass: "container flex al_center"
  }, [_c('div', {
    staticClass: "block-1"
  }, [_c('h1', {
    staticClass: "gold"
  }, [_vm._v("AMASTAR")]), _vm._v(" "), _c('h4', [_vm._v("A blockchain-based platform where you EARN FOR PORN")]), _vm._v(" "), _c('div', {
    staticClass: "form"
  }, [_c('h4', [_vm._v("Get the latest news")]), _vm._v(" "), _c('form', {
    staticClass: "flex",
    attrs: {
      "id": "form"
    }
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.email),
      expression: "email"
    }],
    attrs: {
      "name": "mail",
      "placeholder": "Enter your email address",
      "required": "",
      "type": "text"
    },
    domProps: {
      "value": (_vm.email)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.email = $event.target.value
      }
    }
  }), _vm._v(" "), _c('input', {
    staticClass: "button",
    attrs: {
      "name": "ok",
      "value": "Subscribe"
    },
    on: {
      "click": function($event) {
        _vm.submit()
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "block-2"
  }, [_c('div', {
    staticClass: "block"
  }, [_c('div', {
    staticClass: "blockback"
  }, [_c('h3', [_vm._v("Private pre-sale now live")]), _vm._v(" "), _vm._m(3), _vm._v(" "), _c('div', {
    staticClass: "row flex"
  }, [_c('div', {
    staticClass: "col"
  }, [_c('h4', [_vm._v("1 AS = $0,10")]), _vm._v(" "), _c('button', {
    staticClass: "button",
    on: {
      "click": _vm.showWhitepaper
    }
  }, [_vm._v("Whitepaper")])]), _vm._v(" "), _c('div', {
    staticClass: "col"
  }, [_c('h4', [_vm._v("Bonus from 20%")]), _vm._v(" "), _c('button', {
    staticClass: "button",
    on: {
      "click": _vm.showTerms
    }
  }, [_vm._v("Terms of Sale")])])]), _vm._v(" "), _c('button', {
    staticClass: "button",
    on: {
      "click": _vm.register
    }
  }, [_vm._v("Apply for whitelist")])])]), _vm._v(" "), _vm._m(4)])])]), _vm._v(" "), _vm._m(5), _vm._v(" "), _vm._m(6), _vm._v(" "), _vm._m(7), _vm._v(" "), _vm._m(8), _vm._v(" "), _c('div', {
    staticClass: "div_6",
    attrs: {
      "id": "team"
    }
  }, [_c('div', {
    staticClass: "container"
  }, [_vm._m(9), _vm._v(" "), _c('div', {
    staticClass: "blocks flex"
  }, [_c('div', {
    staticClass: "block"
  }, [_c('div', {
    staticClass: "img open_text",
    on: {
      "mouseenter": function($event) {
        _vm.showCTO = true
      },
      "mouseleave": function($event) {
        _vm.showCTO = false
      }
    }
  }, [(!_vm.showCTO) ? _c('img', {
    attrs: {
      "src": __webpack_require__(285),
      "alt": ""
    }
  }) : _vm._e(), _vm._v(" "), (_vm.showCTO) ? _c('p', [_vm._v(" ergoher gerguher ghverid uh gviguhfg siguhv ridgidu hgridh iduh ridhu vgidsuhgv iuhfgir uhirhu gerdiuh riu gdiu ruhgieruhgieruhg ieruhgiuerh geruhg ierhgi erhgi ehrig ehriughrieirugh eirhuh reg\n                                ")]) : _vm._e()]), _vm._v(" "), _c('h4', [_vm._v("CTO")])]), _vm._v(" "), _c('div', {
    staticClass: "block"
  }, [_c('div', {
    staticClass: "img open_text",
    on: {
      "mouseenter": function($event) {
        _vm.showCEO = true
      },
      "mouseleave": function($event) {
        _vm.showCEO = false
      }
    }
  }, [(!_vm.showCEO) ? _c('img', {
    attrs: {
      "src": __webpack_require__(286),
      "alt": ""
    }
  }) : _vm._e(), _vm._v(" "), (_vm.showCEO) ? _c('p', [_vm._v("ergoher gerguher ghverid uh gviguhfg siguhv ridgidu hgridh iduh ridhu vgidsuhgv iuhfgir uhirhu gerdiuh riu gdiu ruhgieruhgieruhg ieruhgiuerh geruhg ierhgi erhgi ehrig ehriughrieirugh eirhuh reg\n                            ")]) : _vm._e()]), _vm._v(" "), _c('h4', [_vm._v("СEO")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "div_7"
  }, [_c('div', {
    staticClass: "container"
  }, [_vm._m(10), _vm._v(" "), _vm._m(11), _vm._v(" "), _c('div', {
    staticClass: "diagrams flex"
  }, [_c('div', {
    staticClass: "block open_text"
  }, [_c('h4', [_vm._v("Token distribution")]), _vm._v("'\n                        "), _c('pie-chart', {
    attrs: {
      "suffix": "%",
      "defaultFontSize": "18px",
      "colors": ['#f9d554', '#c8c8c8', '#a0a0a0', '#808080', '#404040'],
      "borderWidth": 1,
      "data": [
        ['Pre-ICO and ICO', 60],
        ['Advisors, founders and team', 15],
        ['User reward fund', 10],
        ['Operations', 10],
        ['Bounty and AirDrop', 5]
      ],
      "legend": false
    }
  })], 1), _vm._v(" "), _c('div', {
    staticClass: "block open_text"
  }, [_c('h4', [_vm._v("Fund allocation")]), _vm._v(" "), _c('pie-chart', {
    attrs: {
      "suffix": "%",
      "colors": ['#f9d554', '#c8c8c8', '#a0a0a0', '#808080', '#404040'],
      "data": [
        ['Marketing', 35],
        ['Platform development', 35],
        ['Operations', 25],
        ['Legal and administration', 5]
      ],
      "legend": false
    }
  })], 1)])])]), _vm._v(" "), _vm._m(12), _vm._v(" "), _c('div', {
    staticClass: "div_9",
    attrs: {
      "id": "contact"
    }
  }, [_c('div', {
    staticClass: "container"
  }, [_vm._m(13), _vm._v(" "), _c('div', {
    staticClass: "block"
  }, [_c('h4', [_vm._v("Get in touch and say hi")]), _vm._v(" "), _vm._m(14), _vm._v(" "), _c('div', {
    staticClass: "form"
  }, [_c('h3', [_vm._v("Contact form")]), _vm._v(" "), _c('form', {
    attrs: {
      "id": "form-2"
    }
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.name),
      expression: "name"
    }],
    attrs: {
      "name": "name",
      "placeholder": "Name",
      "required": "",
      "type": "text"
    },
    domProps: {
      "value": (_vm.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.name = $event.target.value
      }
    }
  }), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.sendemail),
      expression: "sendemail"
    }],
    attrs: {
      "name": "mail",
      "placeholder": "Email address",
      "required": "",
      "type": "text"
    },
    domProps: {
      "value": (_vm.sendemail)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.sendemail = $event.target.value
      }
    }
  }), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.message),
      expression: "message"
    }],
    attrs: {
      "name": "message",
      "placeholder": "Message",
      "required": "",
      "type": "text"
    },
    domProps: {
      "value": (_vm.message)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.message = $event.target.value
      }
    }
  }), _vm._v(" "), _c('input', {
    staticClass: "button",
    attrs: {
      "name": "ok",
      "value": "Send"
    },
    on: {
      "click": function($event) {
        _vm.submitmail()
      }
    }
  })])])])])])]), _vm._v(" "), _c('footer', [_c('div', {
    staticClass: "container flex al_center"
  }, [_vm._m(15), _vm._v(" "), _c('ul', {
    staticClass: "menu flex al_center"
  }, [_vm._m(16), _vm._v(" "), _c('li', [_c('a', {
    directives: [{
      name: "smooth-scroll",
      rawName: "v-smooth-scroll"
    }],
    attrs: {
      "href": "#about"
    }
  }, [_vm._v("About")])]), _vm._v(" "), _c('li', [_c('a', {
    directives: [{
      name: "smooth-scroll",
      rawName: "v-smooth-scroll"
    }],
    attrs: {
      "href": "#team"
    }
  }, [_vm._v("Team")])]), _vm._v(" "), _c('li', [_c('a', {
    directives: [{
      name: "smooth-scroll",
      rawName: "v-smooth-scroll"
    }],
    attrs: {
      "href": "#roadmap"
    }
  }, [_vm._v("Roadmap")])])])])]), _vm._v(" "), _c('BackTop'), _vm._v(" "), _c('div', {
    attrs: {
      "id": "menu"
    }
  }, [_c('ul', [_vm._m(17), _vm._v(" "), _vm._m(18), _vm._v(" "), _c('li', [_c('a', {
    directives: [{
      name: "smooth-scroll",
      rawName: "v-smooth-scroll"
    }],
    attrs: {
      "href": "#about"
    }
  }, [_vm._v("About")])]), _vm._v(" "), _c('li', [_c('a', {
    directives: [{
      name: "smooth-scroll",
      rawName: "v-smooth-scroll"
    }],
    attrs: {
      "href": "#team"
    }
  }, [_vm._v("Team")])]), _vm._v(" "), _c('li', [_c('a', {
    directives: [{
      name: "smooth-scroll",
      rawName: "v-smooth-scroll"
    }],
    attrs: {
      "href": "#roadmap"
    }
  }, [_vm._v("Roadmap")])])])])], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "logo"
  }, [_c('a', {
    attrs: {
      "href": "/"
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(245),
      "alt": ""
    }
  })])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('a', {
    attrs: {
      "href": "../../dist/Whitepaper.pdf"
    }
  }, [_vm._v("Whitepaper")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('button', {
    staticClass: " cmn-toggle-switch cmn-toggle-switch__htx"
  }, [_c('span', [_vm._v("menu")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('h4', [_vm._v("Public pre-sale starts in "), _c('span', {
    staticClass: "afss_day_bv"
  }, [_vm._v("0")]), _vm._v(" days "), _c('span', {
    staticClass: "afss_hours_bv"
  }, [_vm._v("00")]), _vm._v(" hours "), _c('span', {
    staticClass: "afss_mins_bv"
  }, [_vm._v("00")]), _vm._v(" minutes")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "social flex al_center"
  }, [_c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(252),
      "alt": ""
    }
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(253),
      "alt": ""
    }
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(248),
      "alt": ""
    }
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(250),
      "alt": ""
    }
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(247),
      "alt": ""
    }
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(249),
      "alt": ""
    }
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(251),
      "alt": ""
    }
  })])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "div_2",
    attrs: {
      "id": "about"
    }
  }, [_c('div', {
    staticClass: "container flex"
  }, [_c('div', {
    staticClass: "text_block"
  }, [_c('div', {
    staticClass: "title"
  }, [_c('h2', [_vm._v("About AmaStar")])]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('h4', [_vm._v("AmaStar is a blockchain-based platform where anyone can upload their own content - and get paid for it! Our unique algorithm will calculate the reward for each content piece based on its popularity. Complete transparency achieved by blockchain - 90% of ad revenue is paid out to content makers. ")])])]), _vm._v(" "), _c('div', {
    staticClass: "video"
  }, [_c('div', {
    staticClass: "block img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(287),
      "width": "400",
      "walt": ""
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(288),
      "width": "400",
      "alt": ""
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(289),
      "width": "400",
      "alt": ""
    }
  })])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "div_3"
  }, [_c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "title flex"
  }, [_c('h2', [_vm._v("One platform for all your adult entertainment")])]), _vm._v(" "), _c('div', {
    staticClass: "blocks flex"
  }, [_c('div', {
    staticClass: "block"
  }, [_c('h4', [_vm._v("Photos")]), _vm._v(" "), _c('p', [_vm._v("Browse a personaliяed feed of amateur nudes based on your preferences")]), _vm._v(" "), _c('p', [_vm._v("Sort by various filters to find exactly what you want to see")]), _vm._v(" "), _c('p', [_vm._v("Upload your own photos and get paid when people like it")])]), _vm._v(" "), _c('div', {
    staticClass: "block"
  }, [_c('h4', [_vm._v("Videos")]), _vm._v(" "), _c('p', [_vm._v("Watch unlimited unique amateur videos from any category, for free")]), _vm._v(" "), _c('p', [_vm._v("Find exactly what you want by applying as many filters as you like")]), _vm._v(" "), _c('p', [_vm._v("Share your own home videos and receive tokens when others watch them")])]), _vm._v(" "), _c('div', {
    staticClass: "block"
  }, [_c('h4', [_vm._v("Live chats")]), _vm._v(" "), _c('p', [_vm._v("Find ladies being naughty and watch them in real time")]), _vm._v(" "), _c('p', [_vm._v("Ask for a private live show just for you")]), _vm._v(" "), _c('p', [_vm._v("Let others watch you live and get paid for every view")])]), _vm._v(" "), _c('div', {
    staticClass: "block"
  }, [_c('h4', [_vm._v("Stories")]), _vm._v(" "), _c('p', [_vm._v("Read erotic sex stories and secret confessions")]), _vm._v(" "), _c('p', [_vm._v("Ask for a specific story just for you from your favorite author")]), _vm._v(" "), _c('p', [_vm._v("Share your fantasies and get paid when others read them")])]), _vm._v(" "), _c('div', {
    staticClass: "block"
  }, [_c('h4', [_vm._v("Games")]), _vm._v(" "), _c('p', {
    staticClass: "pag"
  }, [_vm._v("Play free pornographic flash games with no restrictions")]), _vm._v(" "), _c('p', {
    staticClass: "pag"
  }, [_vm._v("Upload your own games and earn when others play")])])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "div_4"
  }, [_c('div', {
    staticClass: "container flex"
  }, [_c('div', {
    staticClass: "block-1"
  }, [_c('div', {
    staticClass: "title flex"
  }, [_c('h2', [_vm._v("Platform features")])]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_vm._v("We will develop our own blockchain platform with no commissions and high volume of transactions, allowing for efficient distribution of coins. Smart contracts will eliminate any human errors and make using the platform safe and enjoyable for everyone.")])])]), _vm._v(" "), _c('div', {
    staticClass: "block-2"
  }, [_c('div', {
    staticClass: "block"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(277),
      "alt": ""
    }
  })]), _vm._v(" "), _c('h4', [_vm._v("Blockchain-powered")])])]), _vm._v(" "), _c('div', {
    staticClass: "block-2"
  }, [_c('div', {
    staticClass: "block"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(278),
      "alt": ""
    }
  })]), _vm._v(" "), _c('h4', [_vm._v("User-oriented")])])]), _vm._v(" "), _c('div', {
    staticClass: "block-2"
  }, [_c('div', {
    staticClass: "block"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(279),
      "alt": ""
    }
  })]), _vm._v(" "), _c('h4', [_vm._v("Completely transparent")])])]), _vm._v(" "), _c('div', {
    staticClass: "block-2"
  }, [_c('div', {
    staticClass: "block"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(280),
      "alt": ""
    }
  })]), _vm._v(" "), _c('h4', [_vm._v("Totally anonymous")])])]), _vm._v(" "), _c('div', {
    staticClass: "block-2"
  }, [_c('div', {
    staticClass: "block"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(281),
      "alt": ""
    }
  })]), _vm._v(" "), _c('h4', [_vm._v("Secure & reliable")])])]), _vm._v(" "), _c('div', {
    staticClass: "block-2"
  }, [_c('div', {
    staticClass: "block"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(282),
      "alt": ""
    }
  })]), _vm._v(" "), _c('h4', [_vm._v("Fully automatized")])])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "div_5"
  }, [_c('div', {
    staticClass: "container flex"
  }, [_c('div', {
    staticClass: "block-1"
  }, [_c('div', {
    staticClass: "title"
  }, [_c('h2', {
    staticClass: "gold"
  }, [_vm._v("Premium account - enjoy all the perks")])]), _vm._v(" "), _c('div', {
    staticClass: "text text-1"
  }, [_c('h4', [_vm._v("Participate in ICO to receive a free premium account trial")])]), _vm._v(" "), _c('div', {
    staticClass: "text text-2"
  }, [_c('h4', [_vm._v("Contribute 1-10 ETH - "), _c('span', [_vm._v("3 months")])]), _vm._v(" "), _c('h4', [_vm._v("Contribute 10-50 ETH - "), _c('span', [_vm._v("6 month ")])]), _vm._v(" "), _c('h4', [_vm._v("Contribute from 50 ETH - "), _c('span', [_vm._v("1 year")])])]), _vm._v(" "), _c('div', {
    staticClass: "text text-3"
  }, [_c('p', [_vm._v("Premium account features:")]), _vm._v(" "), _c('p', [_vm._v("- Watch with no ads ")]), _vm._v(" "), _c('p', [_vm._v("- Contact any member directly")]), _vm._v(" "), _c('p', [_vm._v("- Ask for personalized content just for you")]), _vm._v(" "), _c('p', [_vm._v("- Unlimited access to private chats ")])])]), _vm._v(" "), _c('div', {
    staticClass: "block-2 flex"
  }, [_c('div', {
    staticClass: "block"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(283),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_c('span', [_vm._v("VIP members Contribute over $10k")])]), _vm._v(" "), _c('p', [_vm._v("- receive invitations to our annual AmaStar parties. Beautiful ladies, food and drinks are on us!")])])]), _vm._v(" "), _c('div', {
    staticClass: "block"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(284),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_c('span', [_vm._v("VIP members Contribute over $50k")])]), _vm._v(" "), _c('p', [_vm._v("- receive invitations to our private AmaStar parties on yachts")])])])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "title flex"
  }, [_c('h2', [_vm._v("CORE TEAM")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "title flex"
  }, [_c('h2', [_vm._v("Token Crowdsale")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "blocks flex al_center"
  }, [_c('div', {
    staticClass: "block"
  }, [_c('h4', [_c('span', [_vm._v("$500k")])]), _vm._v(" "), _c('h4', [_vm._v("Soft cap")])]), _vm._v(" "), _c('div', {
    staticClass: "block"
  }, [_c('h4', [_c('span', [_vm._v("$8mln")])]), _vm._v(" "), _c('h4', [_vm._v("Hard cap")])]), _vm._v(" "), _c('div', {
    staticClass: "block"
  }, [_c('h4', [_c('span', [_vm._v("200mln")])]), _vm._v(" "), _c('h4', [_vm._v("Token supply")])]), _vm._v(" "), _c('div', {
    staticClass: "block"
  }, [_c('h4', [_c('span', [_vm._v("$0.10")])]), _vm._v(" "), _c('h4', [_vm._v("Token price")])]), _vm._v(" "), _c('div', {
    staticClass: "block"
  }, [_c('h4', [_c('span', [_vm._v("AS")])]), _vm._v(" "), _c('h4', [_vm._v("Symbol")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "div_8",
    attrs: {
      "id": "roadmap"
    }
  }, [_c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "title flex"
  }, [_c('h2', [_vm._v("ROADMAP")])]), _vm._v(" "), _c('div', {
    staticClass: "roadmap"
  }, [_c('div', {
    staticClass: "blocks"
  }, [_c('div', {
    staticClass: "row flex"
  }, [_c('div', {
    staticClass: "col"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(246),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_c('span', [_vm._v("Q1 2018")])]), _vm._v(" "), _c('p', [_vm._v("Idea born, market analysis")])])]), _vm._v(" "), _c('div', {
    staticClass: "col"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(246),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_c('span', [_vm._v("Q2 2018")])]), _vm._v(" "), _c('p', [_vm._v("Concept development, team formation")])])]), _vm._v(" "), _c('div', {
    staticClass: "col"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(246),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_c('span', [_vm._v("Q3 2018")])]), _vm._v(" "), _c('p', [_vm._v("Private pre-sale")])])]), _vm._v(" "), _c('div', {
    staticClass: "col"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(246),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_c('span', [_vm._v("October - November 2018")])]), _vm._v(" "), _c('p', [_vm._v("Pre-ICO")])])])]), _vm._v(" "), _c('div', {
    staticClass: "row flex"
  }, [_c('div', {
    staticClass: "col"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(246),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_c('span', [_vm._v("January 2019")])]), _vm._v(" "), _c('p', [_vm._v("Videos added to the platform")])])]), _vm._v(" "), _c('div', {
    staticClass: "col"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(246),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_c('span', [_vm._v("December 2018")])]), _vm._v(" "), _c('p', [_vm._v("ICO")])])]), _vm._v(" "), _c('div', {
    staticClass: "col"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(246),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_c('span', [_vm._v("October 2018")])]), _vm._v(" "), _c('p', [_vm._v("Release of MVP")])])])]), _vm._v(" "), _c('div', {
    staticClass: "row flex"
  }, [_c('div', {
    staticClass: "col"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(246),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_c('span', [_vm._v("February 2019")])]), _vm._v(" "), _c('p', [_vm._v("Live shows added to the platform")])])]), _vm._v(" "), _c('div', {
    staticClass: "col"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(246),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_c('span', [_vm._v("March 2019")])]), _vm._v(" "), _c('p', [_vm._v("Release of own blockchain platform")])])]), _vm._v(" "), _c('div', {
    staticClass: "col"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(246),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_c('span', [_vm._v("May 2019")])]), _vm._v(" "), _c('p', [_vm._v("Release of mobile application")])])]), _vm._v(" "), _c('div', {
    staticClass: "col"
  }, [_c('div', {
    staticClass: "img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(246),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_c('p', [_c('span', [_vm._v("June 2019")])]), _vm._v(" "), _c('p', [_vm._v("Games added to the platform")])])])])])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "title flex"
  }, [_c('h2', [_vm._v("Contact us")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "social flex al_center"
  }, [_c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(252),
      "alt": ""
    }
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(253),
      "alt": ""
    }
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(248),
      "alt": ""
    }
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(250),
      "alt": ""
    }
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(247),
      "alt": ""
    }
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(249),
      "alt": ""
    }
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": ""
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(251),
      "alt": ""
    }
  })])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "logo"
  }, [_c('a', {
    attrs: {
      "href": "index.html"
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(245),
      "alt": ""
    }
  })])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('a', {
    attrs: {
      "href": "../../dist/Whitepaper.pdf"
    }
  }, [_vm._v("Whitepaper")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(245),
      "alt": ""
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('a', {
    attrs: {
      "href": "../../dist/Whitepaper.pdf"
    }
  }, [_vm._v("Whitepaper")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-e0db1226", module.exports)
  }
}

/***/ })

});