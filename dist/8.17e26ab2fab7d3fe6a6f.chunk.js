webpackJsonp([8],{

/***/ 210:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(265)

var Component = __webpack_require__(147)(
  /* script */
  __webpack_require__(256),
  /* template */
  __webpack_require__(290),
  /* scopeId */
  "data-v-020ef21b",
  /* cssModules */
  null
)
Component.options.__file = "/var/www/devico.amastar.net/src/views/dashindex.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] dashindex.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-020ef21b", Component.options)
  } else {
    hotAPI.reload("data-v-020ef21b", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 256:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data() {
        return {
            spanLeft: 5,
            spanRight: 19,
            page: ['about', 'form', 'users']
        };
    },
    computed: {
        iconSize() {
            return this.spanLeft === 5 ? 14 : 24;
        },
        setActive() {
            return this.$route.name.replace('/', '');
        }
    },
    methods: {
        toggleClick() {
            if (this.spanLeft === 5) {
                this.spanLeft = 2;
                this.spanRight = 22;
            } else {
                this.spanLeft = 5;
                this.spanRight = 19;
            }
        },
        routeTo(e) {
            //console.log(e);
            this.$router.push(e);
        }
    }
});

/***/ }),

/***/ 265:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 290:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "layout",
    class: {
      'layout-hide-text': _vm.spanLeft < 5
    }
  }, [_c('Row', {
    attrs: {
      "type": "flex"
    }
  }, [_c('i-col', {
    staticClass: "layout-menu-left",
    attrs: {
      "span": _vm.spanLeft
    }
  }, [_c('Menu', {
    attrs: {
      "active-name": _vm.setActive,
      "theme": "dark",
      "width": "auto"
    },
    on: {
      "on-select": _vm.routeTo
    }
  }, [_c('div', {
    staticClass: "layout-logo-left"
  }, [_c('h3', [_vm._v("Dashboard")])]), _vm._v(" "), _c('Menu-item', {
    attrs: {
      "name": "about"
    }
  }, [_c('Icon', {
    attrs: {
      "type": "ios-navigate",
      "size": _vm.iconSize
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "layout-text"
  }, [_vm._v("main")])], 1), _vm._v(" "), _c('Menu-item', {
    attrs: {
      "name": "form"
    }
  }, [_c('Icon', {
    attrs: {
      "type": "document",
      "size": _vm.iconSize
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "layout-text"
  }, [_vm._v("Subscrubers")])], 1), _vm._v(" "), _c('Menu-item', {
    attrs: {
      "name": "users"
    }
  }, [_c('Icon', {
    attrs: {
      "type": "navicon",
      "size": _vm.iconSize
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "layout-text"
  }, [_vm._v("Users")])], 1)], 1)], 1), _vm._v(" "), _c('i-col', {
    attrs: {
      "span": _vm.spanRight
    }
  }, [_c('div', {
    staticClass: "layout-header"
  }, [_c('i-button', {
    attrs: {
      "type": "text"
    },
    nativeOn: {
      "click": function($event) {
        return _vm.toggleClick($event)
      }
    }
  }, [_c('Icon', {
    attrs: {
      "type": "navicon",
      "size": "32"
    }
  })], 1)], 1), _vm._v(" "), _c('div', {
    staticClass: "layout-breadcrumb"
  }, [_c('Breadcrumb', [_c('Breadcrumb-item', [_vm._v("Main")]), _vm._v(" "), _c('Breadcrumb-item', [_vm._v("Page")]), _vm._v(" "), _c('Breadcrumb-item', [_vm._v(_vm._s(this.$route.name.replace('/', '')))])], 1)], 1), _vm._v(" "), _c('div', {
    staticClass: "layout-content"
  }, [_c('div', {
    staticClass: "layout-content-main"
  }, [_c('transition', {
    attrs: {
      "mode": "out-in"
    }
  }, [_c('router-view')], 1)], 1)]), _vm._v(" "), _c('div', {
    staticClass: "layout-copy"
  }, [_vm._v("\n                2018 © Amastar\n            ")])])], 1)], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-020ef21b", module.exports)
  }
}

/***/ })

});