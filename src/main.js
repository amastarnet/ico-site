import Vue from 'vue';
import iView from 'iview';
import VueRouter from 'vue-router';
import Routers from './router';
import Util from './libs/util';
import App from './app.vue';
import vueSmoothScroll from 'vue-smooth-scroll';
import VueChartkick from 'vue-chartkick';
import Chart from 'chart.js';
import localStorageDB from 'localstoragedb';

import 'iview/dist/styles/iview.css';


const ama = new localStorageDB("amastar", localStorage);
if( ama.isNew() ) {
    // create the "books" table
    ama.createTable("sub", ["email"]);
    ama.createTable("users", ["name","email", "adress", "country", "amount", "password"]);
    ama.commit();
}

Vue.use(VueRouter);
Vue.use(vueSmoothScroll);
Vue.use(iView);
Vue.use(VueChartkick, {adapter: Chart})

// 路由配置
const RouterConfig = {
    mode: 'history',
    routes: Routers
};
const router = new VueRouter(RouterConfig);

router.beforeEach((to, from, next) => {
    iView.LoadingBar.start();
    Util.title(to.meta.title);
    next();
});

router.afterEach((to, from, next) => {
    iView.LoadingBar.finish();
    window.scrollTo(0, 0);
});

new Vue({
    el: '#app',
    router: router,
    render: h => h(App)
});
