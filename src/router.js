const routers = [
    {
        path: '/',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/index.vue'], resolve)
    },
    {
        path: '/sendPass',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/sendPass.vue'], resolve)
    },
    {
        path: '/verify',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/verify.vue'], resolve)
    },
    {
        path: '/congrat',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/congrat.vue'], resolve)
    },  
    {
        path: '/login',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/login.vue'], resolve)
    },
    {
        path: '/register',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/register.vue'], resolve)
    },
    {
        path: '/update',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/update.vue'], resolve)
    },
    {
        path: '/forgot',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/forgot.vue'], resolve)
    },
    {
        path: '/details',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/details.vue'], resolve)
    },

    {
        path: '/adminLogin',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/adminLogin.vue'], resolve)
    },
    {
        path: '/about',
        meta: {
            title: ''
        },
        component: resolve => require(['./views/dashindex.vue'], resolve),
        children: [
            {
                path: '',
                name: "About",
                component: resolve => require(['./views/about.vue'], resolve)
            },
            {
                path: '/form',
                name: "Subscribers",
                component: resolve => require(['./views/form.vue'], resolve)
            },
            {
                path: '/users',
                name: "Users",
                component: resolve => require(['./views/users.vue'], resolve)
            },
        ]
    }

];
export default routers;
